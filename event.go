package pubsub

// A Event represents an message event.
type Event interface {
	String() string // event name
	Event()         // callback event method
}

// Topic is a reference to a PubSub topic.
type Topic struct {
	// The fully qualified identifier for the topic.
	name string
}

// Message represents a Pub/Sub message.
type Message struct {
	ID   string
	Data []byte
}
